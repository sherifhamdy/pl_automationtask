package tests;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import utilities.Helper;
import utilities.PropertyManager;

public class TestBase {
	public static WebDriver driver;

	// String baseURL = dataDriven.urlNameString;
	// String browserName = dataDriven.browserNameString;
	// String OS = dataDriven.OSNameString;

	String baseURL = PropertyManager.getInstance().getExpectedRegisterURL();
	String browserName = PropertyManager.getInstance().getExpectedBrowserName();
	String OS = PropertyManager.getInstance().getExpectedOS();

	@BeforeClass()
	protected void startDriver() {

		if (OS.equalsIgnoreCase("windows")) {

			if (browserName.equalsIgnoreCase("chrome")) {

				System.setProperty("webdriver.chrome.driver",
						System.getProperty("user.dir") + "/drivers/chromedriver.exe");
				driver = new ChromeDriver();

			}

			else if (browserName.equalsIgnoreCase("firefox")) {

				System.setProperty("webdriver.gecko.driver",
						System.getProperty("user.dir") + "/drivers/geckodriver.exe");
				driver = new FirefoxDriver();
			}

		} else if (OS.equalsIgnoreCase("mac")) {

			if (browserName.equalsIgnoreCase("chrome")) {

				System.setProperty("webdriver.chrome.driver",
						System.getProperty("user.dir") + "/drivers/chromedriver_mac");
				driver = new ChromeDriver();

			}

			else if (browserName.equalsIgnoreCase("firefox")) {

				System.setProperty("webdriver.gecko.driver",
						System.getProperty("user.dir") + "/drivers/geckodriver_mac");
				driver = new FirefoxDriver();
			}

		} else if (OS.equalsIgnoreCase("linux")) {

			if (browserName.equalsIgnoreCase("chrome")) {

				System.setProperty("webdriver.chrome.driver",
						System.getProperty("user.dir") + "/drivers/chromedriver_linux");
				driver = new ChromeDriver();

			}

			else if (browserName.equalsIgnoreCase("firefox")) {

				System.setProperty("webdriver.gecko.driver",
						System.getProperty("user.dir") + "/drivers/geckodriver_linux");
				driver = new FirefoxDriver();
			}

		}

		driver.manage().window().maximize();

		driver.navigate().to(baseURL);

	}

	@AfterClass
	protected void exitDriver() {

		driver.quit();

	}

	// Take Screen-shot when test case fails and add it to the screen-shot folder
	@AfterMethod
	protected static void screenshotOnFailure(ITestResult result) throws InterruptedException {
		if (result.getStatus() == ITestResult.FAILURE) {
			try {
				System.out.println("Taking Screenshot....");
				System.out.println("Failed!");
				Helper.captureFAIL_Screenshot(driver, result.getName().concat(" [Failed]"));

			} catch (Exception e) {
				e.printStackTrace();
				System.out.println("Failed to add screenshot in the Report");
			}

		}

	}

	protected String getActualPageTitle() {

		String actual = driver.getTitle();
		return actual;
	}

	protected String getActualPageURL() {
		String actual = driver.getCurrentUrl();
		return actual;
	}

}
