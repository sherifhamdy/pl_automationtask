package pages;

import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class PageBase {

	//protected static WebDriver driver;
	protected static WebDriverWait wait = null;
	protected static Actions actions = null;
	protected static JavascriptExecutor js = null;

	// create constructor
	public PageBase(WebDriver driver) {
		PageFactory.initElements(driver, this);
		wait = new WebDriverWait(driver, 30);
		actions = new Actions(driver);
		js = (JavascriptExecutor) driver;

	}

	// Method to Click Buttons
	protected void clickButton(WebElement button) {

		// actions.click(button);
		// actions.perform();
		button.click();
	}

	// Method to write on field
	protected void setText(WebElement textElement, String value) {

		WaitForElement(textElement);
		textElement.clear();
		textElement.sendKeys(value);
	}


	// Method to wait for an element until visibility
	protected void WaitForElement(WebElement webElement) {
		try {
			wait.until(ExpectedConditions.visibilityOf(webElement));

		} catch (Exception e) {
			System.out.println("ex: " + e);
		}
	}

	// Method to scroll to an element by position
	protected static void scrollByPosition(WebElement element) {

		// get position
		int x = 0;
		int y = element.getLocation().getY();

		// scroll to x y
		js.executeScript("window.scrollBy(" + x + ", " + y + ")");
		// js.executeScript("window.scrollTo("+x+","+y+")");

	}

	// Method to scroll to an element 
	protected void scrollByactions(WebElement element) {
		// actions.moveToElement(element);
		// actions.perform();
		// js.executeScript("document.getElementsByTagName('Button')[1]").;
		// js.executeScript("window.scrollBy(0, " + y + ")");
		// js.executeScript("arguments[0].scrollIntoView(true);", element);
		// ((JavascriptExecutor)
		// driver).executeScript("arguments[0].scrollIntoView(true);", element);
		actions.moveToElement(element).perform();

	}

	// Method to open new tab
	protected void openNewTab(String url) {
		actions.sendKeys(Keys.CONTROL + "t");
		// js.executeScript("arguments[0].scrollIntoView();", element);
		js.executeScript("window.open('" + url + "','_blank');");

	}

}
