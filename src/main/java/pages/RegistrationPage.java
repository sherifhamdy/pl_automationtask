package pages;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class RegistrationPage extends PageBase {

	// Driver linkage
	public RegistrationPage(WebDriver driver) {
		super(driver);

	}

	// Detecting Elements from the UI in Registration screen

	@FindBy(xpath = "//*[@class='cc-compliance']/*[1]")
	public WebElement cookiesBtn;

	@FindBy(xpath = "//input[@name='firstname']")
	public WebElement firstNameElement;

	@FindBy(xpath = "//input[@name='lastname']")
	public WebElement lastNameElement;

	@FindBy(xpath = "//input[@name='email']")
	public WebElement emailElement;

	@FindBy(xpath = "//input[@name='password']")
	public WebElement passwordElement;

	@FindBy(xpath = "(//*[@type='submit'])[1]")
	public WebElement buttonSubmitElement;
	//// *[@type='submit'], ////*[@type='submit'])[1]

	@FindBy(xpath = "//*[@id='login']")
	public WebElement registrationForm;

	@FindBy(xpath = "//input[@name='confirmpassword']")
	public WebElement confpass;

	@FindBy(xpath = "//input[@name='phone']")
	public WebElement mobilenum;

	@FindBy(xpath = "//*[@id='headersignupform']//p")
	public WebElement errorMsg;

	@FindBy(xpath = "//*[@class='resultsignup']/*[1]")
	public WebElement errorMsgEmailExist;

	@FindBy(xpath = "//*[@id='headersignupform']//p/..")
	public WebElement errorMsgList;

	@FindBy(xpath = "//*[@id='headersignupform']//p")
	public List<WebElement> errorMsgLists;

	// Register user in the system and validate all elements are displayed true
	public boolean registerContentIsDisplayed() {

		boolean flag = false;

		WaitForElement(registrationForm);

		if (firstNameElement.isDisplayed() && lastNameElement.isDisplayed() && emailElement.isDisplayed()
				&& passwordElement.isDisplayed() && buttonSubmitElement.isDisplayed()) {
			flag = true;

		} else {
			flag = false;

		}
		return flag;
	}

	public String newFullRegister(String FirstNameField, String LastNameField, String EmailField, String PasswordField,
			String ConfirmPasswordField, String MobileNumField) throws InterruptedException {
		String resultString = "";

		registerationSteps(FirstNameField, LastNameField, EmailField, PasswordField, ConfirmPasswordField,
				MobileNumField);
		Thread.sleep(2000);
		try {
			if (errorMsgEmailExist.isDisplayed()) {
				// WaitForElement(errorMsgEmailExist);
				resultString = errorMsgEmailExist.getText();
			} else if (errorMsg.isDisplayed()) {
				WaitForElement(errorMsg);
				resultString = errorMsg.getText();
			}

		} catch (Exception e) {
			if (errorMsg.isDisplayed()) {
				resultString = errorMsg.getText();
			} else {
				System.out.println("ex: " + e);

			}
		}

		return resultString;

	}

	public void successfullRegister(String FirstNameField, String LastNameField, String EmailField,
			String PasswordField, String ConfirmPasswordField, String MobileNumField) throws InterruptedException {

		try {
			registerationSteps(FirstNameField, LastNameField, EmailField, PasswordField, ConfirmPasswordField,
					MobileNumField);
		} catch (Exception e) {
			System.out.println("ex: " + e);
		}

	}

	// Send values to fields and submit
	private void registerationSteps(String firstname, String lastname, String email, String password,
			String confirmpass, String mobilenumber) {

		if (cookiesBtn.isDisplayed()) {
			clickButton(cookiesBtn);
		}
		// scrollTop();
		setText(firstNameElement, firstname);
		setText(lastNameElement, lastname);
		setText(emailElement, email);
		setText(passwordElement, password);
		setText(confpass, confirmpass);
		setText(mobilenum, mobilenumber);
		scrollByactions(buttonSubmitElement);
		// scrollByPosition(buttonSubmitElement);
		WaitForElement(buttonSubmitElement);
		clickButton(buttonSubmitElement);

	}
}
